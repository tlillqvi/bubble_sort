#include <algorithm>
#include <vector>
#include <iostream>
#include <iterator>

std::vector<int> sort(std::vector<int> input)
{
  for(uint i = 0; i < input.size()-1; i++)
    {
      for(uint j = i+1; j < input.size(); j++)
	if(input[i] > input[j]) std::swap(input[i],input[j]);
    }

  return input;
}

int main()
{
  using namespace std;
  int arr_i[] = { 5, 3, 4, 2, 1 };
  std::vector<int> arr( arr_i, arr_i + 5 );
  
  ostream_iterator<int> out_it(cout," ");
  copy( arr.begin(), arr.end(), out_it );
  cout << endl;

  arr = sort(arr);
  
  copy( arr.begin(), arr.end(), out_it );
  cout << endl;
}
