CXXFLAGS=-g -Wall -std=c++11
CXX=g++

bubble_sort: bubble_sort.o
	$(CXX) $< -o $@

bubble_sort.o: bubble_sort.cpp

